#include <avr/io.h>
//#include "LCD/lcd44780.h"
#include <avr/interrupt.h>
#include <util/delay.h>

#define F_START F_CPU/8000*13.5
#define F_ONE F_CPU/8000*2.25
#define F_ZERO F_CPU/8000*1.125
#define F_MARGIN F_CPU/8000*0.4

volatile uint8_t frame_ready;
volatile uint8_t address;
volatile uint8_t command;

#define LED1 (1<<PD7)
#define LED2 (1<<PC0)

// definicje klawiszy
#define UP 		98
#define LEFT 	32
#define OK 		2
#define RIGHT 	194
#define DOWN 	168
#define NUM0 	74
#define NUM1 	104
#define NUM2 	152
#define NUM3 	176
#define NUM4 	48
#define NUM5 	24
#define NUM6 	122
#define NUM7 	16
#define NUM8 	56
#define NUM9 	90
#define STAR 	66
#define HASH 	82
// koniec definicji klawiszy

int main(void)
{
	DDRD |= LED1;
	PORTD &= ~ LED1;

	DDRC |= LED2;
		PORTC &= ~ LED2;
//        DDRA |= (1<<PA7);
//        PORTA |= (1<<PA7); /* pod�wietlenie wy�wietlacza LCD */

        PORTD |= (1<<PD6); /* podci�gni�cie wej�cia IR */


//        lcd_init();     /* inicjalizacja LCD */
        /* ir timer */
        TCCR1B |= (1<<CS11);    /* prescaler = 8 */
        TCCR1B &= ~(1<<ICES1);  /* falling edge */
        TIMSK |= (1<<TICIE1);   /* enable interrupt */
        sei();
        while(1)
        {

//        	_delay_ms(1000);


           if(frame_ready)
           {
//        	   for (int i = 0; i<command; i++) {
//        		   PORTD ^= LED1;
//        		   _delay_ms(500);
//        	   }
        	   if(command == 82) PORTD ^= LED1;
        	   if(command == UP) PORTC ^= LED2;
//              lcd_locate(0,0);
//              lcd_int(address);
////              lcd_str("  ");
//              lcd_locate(1,0);
//              lcd_int(command);
//              lcd_str("  ");
              frame_ready = 0;

           }
        }
}

ISR(TIMER1_CAPT_vect)
{
        static uint32_t temp_frame;
        static uint16_t last_icr;
        uint16_t width;
        static uint8_t frame_idx = 33;
        uint8_t bit = 2;
        width = ICR1 - last_icr;
        last_icr = ICR1;
        if(frame_idx == 33 && width > F_START - F_MARGIN && width < F_START + F_MARGIN)
        {
                frame_idx--;
        }
        if(frame_idx < 32)
        {
                if(width > F_ONE - F_MARGIN && width < F_ONE + F_MARGIN) bit = 1;
                if(width > F_ZERO - F_MARGIN && width < F_ZERO + F_MARGIN) bit = 0;
                if(bit != 2)
                {
                        temp_frame |= (uint32_t)bit << frame_idx;
                        if(frame_idx == 0)
                        {
                                address = (uint8_t)(temp_frame >> 24);
                                command = (uint8_t)(temp_frame >> 8);
                                temp_frame = 0;
                                frame_idx = 33;
                                frame_ready = 1;
                        }
                        else
                        {
                                frame_idx--;
                        }
                }
                else
                {
                        frame_idx = 33;
                }
        }
        if(frame_idx == 32) frame_idx--;
}
